﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCParticles : MonoBehaviour
{
	[SerializeField]
	private ParticleSystem m_deathParticlePrefab;
	
	// Use this for initialization
	void Start ()
	{
		GetComponent<IHealth>().OnNPCDied += HandleOnNPCDied;
	}

	private void HandleOnNPCDied()
	{
		var deathParticle = Instantiate(m_deathParticlePrefab, transform.position, transform.rotation);
		Destroy(deathParticle, 4f);
	}
}
