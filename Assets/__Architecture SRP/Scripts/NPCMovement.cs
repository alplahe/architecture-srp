﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NPCMovement : MonoBehaviour
{
	[SerializeField] private float m_speed = 5;
	private Rigidbody m_rigidbody;
	private Collider m_collider;
	private MeshRenderer m_meshRenderer;
	private Transform m_transform;
	private GameObject childGameObject;

	// Use this for initialization
	void Start ()
	{
		InitializeComponents();
	}

	private void InitializeComponents()
	{
		IfThereIsNotChildGameObjectOrIsACameraCreateOne();

		IfThereIsNoRigidbodyInChildAddOne();
	
		GetComponentsInChild();
	}
	
	private void IfThereIsNotChildGameObjectOrIsACameraCreateOne()
	{
		if (IsThereNoChild() || IsNotANPCChild())
		{
			childGameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			childGameObject.transform.position = transform.position;
			childGameObject.AddComponent<Rigidbody>();
			childGameObject.tag = "NPC_Child";
			AttachCreatedGameObjectAs(childGameObject);
		}
	}

	private bool IsThereNoChild()
	{
		return gameObject.transform.childCount != 1;
	}

	private bool IsNotANPCChild()
	{
		return !gameObject.transform.GetChild(0).CompareTag("NPC_Child");
	}

	private void AttachCreatedGameObjectAs(GameObject childGO)
	{
		childGO.transform.parent = gameObject.transform;
	}

	private void IfThereIsNoRigidbodyInChildAddOne()
	{
		if (GetComponentInChildren<Rigidbody>() == null)
		{
			gameObject.transform.GetChild(0).transform.gameObject.AddComponent<Rigidbody>();
		}
	}

	private void GetComponentsInChild()
	{
		m_rigidbody = GetComponentInChildren<Rigidbody>();
		m_collider = GetComponentInChildren<Collider>();
		m_meshRenderer = GetComponentInChildren<MeshRenderer>();
		m_transform = gameObject.transform.GetChild(0);
	}

	// Update is called once per frame
	void Update ()
	{
		Move();
	}

	private void Move()
	{
		MoveHorizontally();
		MoveVertically();
	}

	private void MoveHorizontally()
	{
		float inputHorizontal = Input.GetAxis("Horizontal");
		if (inputHorizontal != 0)
		{
			TranslateAlongAxis(new Vector3(1, 0, 0), inputHorizontal);
			ResetForce();
		}
	}

	private void MoveVertically()
	{
		float inputVertical = Input.GetAxis("Vertical");
		if (inputVertical != 0)
		{
			TranslateAlongAxis(new Vector3(0, 0, 1), inputVertical);
			ResetForce();
		}
	}

	private void TranslateAlongAxis(Vector3 axisOfMovement, float inputAxis)
	{
		transform.Translate(axisOfMovement * m_speed * inputAxis * Time.deltaTime);
	}
	
	private void ResetForce()
	{
//		m_rigidbody.velocity = Vector3.zero;
//		m_rigidbody.angularVelocity = Vector3.zero;
		m_rigidbody.velocity = new Vector3(0, m_rigidbody.velocity.y, 0);
		m_rigidbody.angularVelocity = new Vector3(0, m_rigidbody.angularVelocity.y, 0);
	}
}
