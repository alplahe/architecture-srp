﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	[SerializeField] private float m_speed = 5;
	private Rigidbody m_rigidbody;

	// Use this for initialization
	void Start ()
	{
		m_rigidbody = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update ()
	{
		Move();
	}

	private void Move()
	{
		float inputHorizontal = Input.GetAxis("Horizontal");
		if (inputHorizontal != 0)
		{
			m_rigidbody.AddForce(new Vector3(1,0,0) * inputHorizontal * Time.deltaTime, ForceMode.Impulse);
		}
		
		float inputVertical = Input.GetAxis("Vertical");
		if (inputVertical != 0)
		{
			m_rigidbody.AddForce(new Vector3(0,0,1) * inputVertical * Time.deltaTime, ForceMode.Impulse);
		}
	}
}
