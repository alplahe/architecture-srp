﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	[SerializeField] private GameObject m_NPC;
	[SerializeField] private List<GameObject> m_gameObjectsToInstantiate;

	// Use this for initialization
	void Start ()
	{
		InstantiateNPC();

		InstantiateGameObjects();
	}

	private void InstantiateNPC()
	{
		m_NPC = Instantiate(m_NPC);
	}

	private void InstantiateGameObjects()
	{
		foreach (GameObject gameObjectToInstantiate in m_gameObjectsToInstantiate)
		{
			Instantiate(gameObjectToInstantiate);
		}
	}

	public void TakeDamage(int amount)
	{
		m_NPC.GetComponent<NPC>().TakeDamage(amount);
	}
}
