﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public GameObject m_player;
	private Vector3 m_offset;

	// Use this for initialization
	void Start ()
	{
		CalculateOffset();
	}

	private void CalculateOffset()
	{
		m_offset = transform.position - m_player.transform.position;
		Debug.Log(m_offset);
	}

	// LateUpdate is called after Update each frame
	void LateUpdate ()
	{
		ApplyOffset();
	}

	private void ApplyOffset()
	{
		transform.position = m_player.transform.position + m_offset;
	}
}