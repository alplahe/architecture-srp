﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
	private Slider m_slider;
	private const float m_SliderStartingValue = 1;

	// Use this for initialization
	void Start ()
	{
		m_slider = GetComponentInChildren<Slider>();
		m_slider.value = m_SliderStartingValue;
		GetComponentInParent<IHealth>().OnHPPercentChanged += HandleOnHPPercentChanged;
	}

	private void HandleOnHPPercentChanged(float percent)
	{
		m_slider.value = percent;
	}
}
