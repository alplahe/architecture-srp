﻿using System;
using UnityEngine;

public class StandardHealth : MonoBehaviour, IHealth
{
	[SerializeField]
	private int m_startingHp = 10;

	private int m_currentHp;

	public event Action<float> OnHPPercentChanged = delegate { };
	public event Action OnNPCDied = delegate { };

	private float CurrentHpPercent
	{
		get
		{
			return (float)m_currentHp / (float)m_startingHp;
		}
	}

	private void Start()
	{
		m_currentHp = m_startingHp;
	}

	public void TakeDamage(int amount)
	{
		if (amount <= 0)
			throw new ArgumentOutOfRangeException("Invalid Damage amount specified: " + amount);

		m_currentHp -= amount;

		OnHPPercentChanged(CurrentHpPercent);

		if (m_currentHp <= 0)
			Die();
	}

	private void Die()
	{
		OnNPCDied();
		GameObject.Destroy(this.gameObject);
	}
}