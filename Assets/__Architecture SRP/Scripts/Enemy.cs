﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	[SerializeField] private int m_damage;
	private GameObject m_gameManager;

	private void Start()
	{
		m_gameManager = GameObject.FindGameObjectWithTag("GameManager");
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag("NPC_Child"))
		{
			m_gameManager.GetComponent<GameManager>().TakeDamage(m_damage);
		}
	}
}
