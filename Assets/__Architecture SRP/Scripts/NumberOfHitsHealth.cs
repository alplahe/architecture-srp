﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberOfHitsHealth : MonoBehaviour, IHealth
{
	[SerializeField] private int m_healthInHits = 5;
	[SerializeField] private float m_invulnerabilityTimeAfterEachHit = 5f;

	private int m_hitsRemaining;
	private bool m_canTakeDamage = true;
	
	public event Action<float> OnHPPercentChanged = delegate { };
	public event Action OnNPCDied = delegate { };
	
	private float CurrentHpPercent
	{
		get
		{
			return (float)m_hitsRemaining / (float)m_healthInHits;
		}
	}

	private void Start()
	{
		m_hitsRemaining = m_healthInHits;
	}

	public void TakeDamage(int amount)
	{
		if (m_canTakeDamage)
		{
			StartCoroutine(InvulnerabilityTimer());

			m_hitsRemaining --;

			OnHPPercentChanged(CurrentHpPercent);

			if (m_hitsRemaining <= 0)
				Die();
		}
	}

	private IEnumerator InvulnerabilityTimer()
	{
		m_canTakeDamage = false;
		yield return new WaitForSeconds(m_invulnerabilityTimeAfterEachHit);
		m_canTakeDamage = true;
	}

	private void Die()
	{
		OnNPCDied();
		GameObject.Destroy(this.gameObject);
	}
}