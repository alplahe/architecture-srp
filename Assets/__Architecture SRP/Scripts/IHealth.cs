﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealth
{
	event System.Action<float> OnHPPercentChanged;
	event System.Action OnNPCDied;
	void TakeDamage(int amount);
}
